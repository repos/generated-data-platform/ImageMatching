# ImageMatching
Image recommendation for unillustrated Wikipedia articles.

This repo is a refactoring of https://github.com/mirrys/ImageMatching. It was developed for the [Image Suggestion](https://phabricator.wikimedia.org/project/view/5098/) proof of concept. 

# Installation

The package is published to a local `pypi` index at https://gitlab.wikimedia.org/api/v4/projects/40/packages/pypi/simple

You can install it at the command line with `pip`
``` shell
pip install algorunner --extra-index-url https://gitlab.wikimedia.org/api/v4/projects/40/packages/pypi/simple
```

Or add it as a `requirement.txt` dependency with:
```
-i https://gitlab.wikimedia.org/api/v4/projects/40/packages/pypi/simple
algorunner
```

## Getting started

Connect to stat1005 through ssh (the remote machine that will host your notebooks)
```
ssh stat1005.eqiad.wmnet
```


### Build from source
First, clone the repository
```shell
git clone git@gitlab.wikimedia.org:repos/generated-data-platform/ImageMatching.git
```

Setup and activate the virtual environment
```shell
cd ImageMatching
virtualenv -p python3 venv
source venv/bin/activate
```

Install the dependencies
```shell
export http_proxy=http://webproxy.eqiad.wmnet:8080
export https_proxy=http://webproxy.eqiad.wmnet:8080
make wheel
pip install dist/algorunner-0.2.0-py3-none-any.whl --no-cache-dir
```

`scripts` are installed at `./venv/bin/` and automatically added to PATH.

### Running the script

To run the script pass in the **snapshot** (required), **language** (defaults to all wikis),
and **output directory** (defaults to Output)
```shell
algorunner.py 2020-12-28 hywiki Output
```

The output .ipynb and .tsv files can be found in your output directory
```shell
ls Output
hywiki_2020-12-28.ipynb  hywiki_2020-12-28_wd_image_candidates.tsv
```
