spark_version := 2.4.8
hadoop_version := 2.7
spark_home := spark-${spark_version}-bin-hadoop${hadoop_version}
spark_tgz_url := https://downloads.apache.org/spark/spark-${spark_version}/${spark_home}.tgz
ima_notebook := ima/notebooks/algorithm.ipynb
pypirc := .pypirc
pypi_repo := gitlab

venv: requirements.txt
	test -d venv || python3 -m venv venv
	. venv/bin/activate; pip3 install --upgrade pip; pip3 install -Ur requirements.txt;
py:	venv
	rm -rf build
	# nbconvert output is saved as <basename>.py
	. venv/bin/activate; jupyter nbconvert ${ima_notebook} --to script --output-dir='./build'

wheel:	venv
	. venv/bin/activate; python3 setup.py bdist_wheel

publish:	wheel
	. venv/bin/activate; python3 -m twine upload --repository ${pypi_repo} dist/* --config-file ${pypirc} --verbose
